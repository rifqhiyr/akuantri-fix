import React, { Component } from 'react'
import "./HomePage.css"

class HomePage extends Component {
  render() {
    return (
      <div>
        <nav className="navbar fixed-top navbar-light bg-light">
          <div className="container">
            <a className="navbar-brand" href><img src={require("../image/logo.png")} width={210} height={50} className="d-inline-block align-top" alt="" /></a>
            <ul className="nav justify-content-end">
              <li className="nav-item">
                <a className="nav-link active" href>Home</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href>Layanan</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href>FAQ</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href>Blog</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href>Tentang Kami</a>
              </li>
              <button type="button" className="btn btn-success">Daftar Liner</button>
            </ul>
          </div>
        </nav>
        {/* Navigation */}
        {/* Jumbotron */}
        <section className="banner-akuantri">
          <div className="container">
            <div className="row">
              <div className="col">
                <h1>Akuantri Indonesia</h1>
                <p>
                  Kami sadar menunggu adalah hal yang membosankan, dan kami juga
                  sadar akan ada banyak waktu terbuang begitu saja karna harus
                  menunggu. Untuk itu, Akuantri hadir untuk membuat waktu berharga
                  Anda tidak hilang begitu saja. Kami akan menggantikan antrian
                  Anda, karna kami sadar bahwa Waktu Anda sangat Berharga.
                </p>
                <a href>
                  <img className="gplay" src={require("../image/gplaylogo.png") } alt="" />
                </a>
                <a href>
                  <img className="button1" src={require("../image/pelajarilogo.png")} alt="" />
                </a>
              </div>
            </div>
          </div>
        </section>
        {/* Jumbotron */}
        {/* Feature */}
        <section className="feature-akuantri">
          <div className="container">
            <div className="row">
              <div className="col">
                <div className="feature-title">
                  <h2>Berbagai Jenis antrian</h2>
                  <p>
                    Jangan habiskan waktu Anda hanya untuk mengantri.
                  </p>
                </div>
                <div className="feature-content">
                  <span>
                    <i className="fa fa-check" />
                  </span>
                  <p className="content-title">Layanan Kesehatan</p>
                  <p className="content-desc">
                    Antri di Rumah Sakit, Klinik, dan Layanan kesehatan lain kini
                    bisa lebih menyenangkan.
                  </p>
                </div>
                <div className="feature-content">
                  <span>
                    <i className="fa fa-check" />
                  </span>
                  <p className="content-title">Berbagai Tiket Box</p>
                  <p className="content-desc">
                    Tidak takut lagi kehabisan Tiket atau Diskon menarik dengan
                    layanan Akuantri.
                  </p>
                </div>
                <div className="feature-content">
                  <span>
                    <i className="fa fa-check" />
                  </span>
                  <p className="content-title">Berbagai Restaurant</p>
                  <p className="content-desc">
                    angan lewatkan Diskon atau Opening Restaurant kesuakaan Anda,
                    Kami yang akan Antri.
                  </p>
                </div>
                <div className="feature-content">
                  <span>
                    <i className="fa fa-check" />
                  </span>
                  <p className="content-title">Salon atau Barbershop</p>
                  <p className="content-desc">
                    Kami yang akan menunggu giliran Anda di Salon atau Barbershop
                    Kesayangan Anda.
                  </p>
                </div>
                <div className="feature-content">
                  <span>
                    <i className="fa fa-check" />
                  </span>
                  <p className="content-title">Layanan Pemerintahan atau Bank</p>
                  <p className="content-desc">
                    Tidak bisa datang pagi untuk Antri? Kami yang akan menggantikan
                    Antrian Anda.
                  </p>
                </div>
                <div className="feature-content">
                  <span>
                    <i className="fa fa-check" />
                  </span>
                  <p className="content-title">Jenis Antrian Lain</p>
                  <p className="content-desc">
                    Antrian tidak masuk kategori? Ceritakan tentang jenis Antrian
                    Anda kepada kami.
                  </p>
                </div>
              </div>
              <div className="col">
                <img className="hp" src={require("../image/gambarhp.png")} alt="" />
              </div>
            </div>
          </div>
        </section>
        {/* Feature */}
        {/* FAQ */}
        <section className="faq">
          <div className="container">
            <div className="row">
              <div className="col-12">
                <div className="header">
                  <h2>Frequently Ask Question</h2>
                  <p>pertanyaan yang sering ditanyakan tentang Akuantri</p>
                </div>
              </div>
              <div className="col-6">
                <div className="accordion" id="accordionExample">
                  <div className="card">
                    <div className="card-header" id="headingOne">
                      <h2 className="mb-0">
                        <button className="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          Apa &amp; siapa itu Liner Akuantri?
                        </button>
                      </h2>
                    </div>
                    <div id="collapseOne" className="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                      <div className="card-body">
                        Mitra akuantri yang ada di lapangan untuk
                        mengantri/mengantikan posisi antrian Anda.
                      </div>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-header" id="headingTwo">
                      <h2 className="mb-0">
                        <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          Jasa apa saja yang diberikan Akuantri
                        </button>
                      </h2>
                    </div>
                    <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div className="card-body">
                        <p>
                          Jasa Layanan Tunggu dan Antrian diantaranya sebagai
                          berikut :
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-header" id="headingThree">
                      <h2 className="mb-0">
                        <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          Bagaimana cara pemesanan Liner Akuantri?
                        </button>
                      </h2>
                    </div>
                    <div id="collapseThree" className="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                      <div className="card-body">
                        <p>
                          Sangat mudah, silahkan Download Aplikasi Akuantri di
                          Google Play. Setelah itu klik Icon Antrian apa yang Anda
                          butuhkan, isi data seperti lokasi antrian, kapan tanggal
                          akan dilakukan antrian, Jam untuk mulai mengantri, pesan
                          tambahan atau informasi tambahan tentang antrian Anda.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-6">
                <div className="accordion" id="accordionExample">
                  <div className="card">
                    <div className="card-header" id="headingOne">
                      <h2 className="mb-0">
                        <button className="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          Collapsible Group Item #1
                        </button>
                      </h2>
                    </div>
                    <div id="collapseOne" className="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                      <div className="card-body">
                        
                      </div>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-header" id="headingTwo">
                      <h2 className="mb-0">
                        <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          Collapsible Group Item #2
                        </button>
                      </h2>
                    </div>
                    <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                      <div className="card-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life
                        accusamus terry richardson ad squid. 3 wolf moon officia
                        aute, non cupidatat skateboard dolor brunch. Food truck
                        quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,
                        sunt aliqua put a bird on it squid single-origin coffee
                        nulla assumenda shoreditch et. Nihil anim keffiyeh
                        helvetica, craft beer labore wes anderson cred nesciunt
                        sapiente ea proident. Ad vegan excepteur butcher vice lomo.
                        Leggings occaecat craft beer farm-to-table, raw denim
                        aesthetic synth nesciunt you probably haven't heard of them
                        accusamus labore sustainable VHS.
                      </div>
                    </div>
                  </div>
                  <div className="card">
                    <div className="card-header" id="headingThree">
                      <h2 className="mb-0">
                        <button className="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          Collapsible Group Item #3
                        </button>
                      </h2>
                    </div>
                    <div id="collapseThree" className="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                      <div className="card-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life
                        accusamus terry richardson ad squid. 3 wolf moon officia
                        aute, non cupidatat skateboard dolor brunch. Food truck
                        quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,
                        sunt aliqua put a bird on it squid single-origin coffee
                        nulla assumenda shoreditch et. Nihil anim keffiyeh
                        helvetica, craft beer labore wes anderson cred nesciunt
                        sapiente ea proident. Ad vegan excepteur butcher vice lomo.
                        Leggings occaecat craft beer farm-to-table, raw denim
                        aesthetic synth nesciunt you probably haven't heard of them
                        accusamus labore sustainable VHS.
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* FAQ */}
        {/* Testimonial */}
        <section className="testimonial">
          <div className="container">
            <div className="row">
              <div className="col-12">
                <div className="header">
                  <h2>Testimonial</h2>
                  <p>Mereka yang telah menghemat waktunya untuk mengantri.</p>
                </div>
              </div>
              <div className="col-12">
                <div className="card-deck">
                  <div className="card">
                    <img src= {require("../image/testipoto4.png")} className="card-img-top" alt="..." />
                  </div>
                  <div className="card">
                    <img src={require("../image/testipoto2.png")} className="card-img-top" alt="..." />
                  </div>
                  <div className="card">
                    <img src={require("../image/testipoto3.png")} className="card-img-top" alt="..." />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* Testimonial */}
        {/* product */}
        <section className="product">
          <div className="container">
            <div className="row">
              <div className="col">
                <div className="card-deck">
                  <div className="card">
                    <div style={{fontSize: '20px'}}>
                      <i className="fa fa-stopwatch fa-10x" style={{color: '#00B906'}} />
                    </div>
                    <div className="card-body">
                      <h5 className="card-title">Hemat Waktu Anda</h5>
                      <p className="card-text">
                        Bebaskan dan hemat waktu Anda untuk Antri, Kami yang akan
                        menggantikan posisi Antrian panjang Anda.
                      </p>
                    </div>
                  </div>
                  <div className="card">
                    <span style={{fontSize: '20px'}}><i className="fa fa-calendar fa-10x" style={{color: '#00B906'}} /></span>
                    <div className="card-body">
                      <h5 className="card-title">Pesan Antrian Kapan Saja</h5>
                      <p className="card-text">
                        Silahkan pesan Antrian untuk sekarang , Besok, Minggu depan,
                        kapan Saja. Liner kami akan siap Antri untuk Anda.
                      </p>
                    </div>
                  </div>
                  <div className="card">
                    <span style={{fontSize: '20px'}}><i className="fa fa-ticket-alt fa-10x" style={{color: '#00B906'}} /></span>
                    <div className="card-body">
                      <h5 className="card-title">Berbagai Jenis Antrian</h5>
                      <p className="card-text">
                        Beragam jenis Antrian tinggal pesan seperti Antri BPJS,
                        Imigrasi, Pembayaran pajak, dan Antrian lainya.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* product */}
        {/* Footer */}
        <section className="footer">
          <div className="container">
            <div className="row">
              <div className="col-sm-4">
                <h5>SEKILAS AKUANTRI</h5>
                <p>
                  Akuantri adalah Aplikasi Layaan Jasa Antri yang akan menghemat
                  waktu Anda dalam hal mengantri. Kami mempunyai jenis Antrian yang
                  beragam. dengan Akuantri Anda bisa melakukan hal yang lebih
                  penting, sementara antrian Anda akan tetap berajalan.
                </p>
              </div>
              <div className="col-sm-4 link-footer">
                <h5>LINKS</h5>
                <ul>
                  <li><a href>Tentang Kami</a></li>
                  <li><a href>Promo</a></li>
                  <li><a href>Karir</a></li>
                  <li><a href>Hubungi kami</a></li>
                  <li><a href>Syarat &amp; ketentuan</a></li>
                </ul>
              </div>
              <div className="col-sm-4">
                <h5>Download Aplikasi</h5>
                <a href><img className="gplay" src={require("../image/gplaylogo.png")} alt="" /></a>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-8">
                <small className="block">© PT Aku Antri Indonesia. All Rights Reserved.</small>
                <div>
                  <small className="block">Jasa Layanan Antri Indonesia - Akuantri</small>
                </div>
              </div>
              <div className="col-sm-4">
                <span><i className="fa fa-twitter-square" style={{color: '#00B906'}} /></span>
                <span><i className="fa fa-facebook-square" style={{color: '#00B906'}} /></span>
                <span><i className="fa fa-youtube-square" style={{color: '#00B906'}} /></span>
                <span><i className="fa fa-instagram" style={{color: '#00B906'}} /></span>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
  }
}

export default HomePage;